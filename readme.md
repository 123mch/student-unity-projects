| Project | Progress |
| ------- | -------- |
| chess | not done |
| backgammon | partially done |
| pacman | partially done |
| rider | partially done |
| tetris | almost done |
| color switch | almost done |
| don't touch the spikes | almost done |
| damka | done teaching |
| fireboy and watergirl | done teaching |
| mario | done teaching |
| jetpack joyride | working on book(1/2) |
| subway surfs | working on book |
| icy towers | working on book |
| bbtan | working on book |
| crossy road | working on book |
